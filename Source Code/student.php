<link rel="stylesheet" href="theme.css"/>

<link rel="stylesheet" href="bootstrap.css"/>
<?php include('header.php')?>	
<?php 
	include("databaseconnection.php"); 
	 include_once("addheader.php"); 
	if(!isset($_SESSION['id'])) { header("Location: index.php");}
	
	
?>


<?php
	 if ( isset($_GET["StudentID"]) && isset($_GET["Firstname"]) && isset($_GET["Lastname"])  && isset($_GET["Batch"])  && isset($_GET["DEPT"]) && isset($_GET["zone"]) && isset($_GET["unit"]) && isset($_GET["BloodGroup"]) && isset($_GET["ContactNo"]) && isset($_GET["HallRoomNo"]) && isset($_GET["LDD"])  )
 {
	 $StudentID =   $_GET["StudentID"];
	 $Firstname = strtolower($_GET["Firstname"]);
	 $Lastname = strtolower($_GET["Lastname"]);
	 $DEPT = $_GET["DEPT"];
	 $zone = strtoupper($_GET["zone"]);
			
	 $unit = strtoupper($_GET["unit"]);
	 $BloodGroup = $_GET["BloodGroup"];
	 $LastDonationDate = $_GET["LDD"];
	 $date = new DateTime($LastDonationDate);
	 $ldd =  $date->format('d-M-Y');
	 $ContactNo = $_GET["ContactNo"];
	 $Batch = $_GET["Batch"];
	 $HallRoomNo = $_GET["HallRoomNo"];
	 
	 
	 
			
			
			$query = "select donorid from donor where donorid = $StudentID";
			
			$stid = oci_parse($conn,$query);
			if($query_run = oci_execute($stid))
			{
				$results=array();
				$numrows = oci_fetch_all($stid, $results, null, null, OCI_FETCHSTATEMENT_BY_ROW);
				if($numrows > 0)
				{
					echo "donor of this studentID is already inserted";
					die();
				}
				oci_free_statement($stid);
			}
			
			
			
			$query_donor = "insert into donor(DonorID,Firstname,Lastname,DEPT,Zone,Unit,Lastdonationdate,ContactNo,B_ID) values ( ".$StudentID.",'".$Firstname."','".$Lastname."','".$DEPT."','".$zone."','".$unit."','".$ldd." ',' "
			.$ContactNo."',".$StudentID.")";
			//echo $query_donor;
			//die();
			$query_student = " insert into student values (".$StudentID.",".$Batch.",'".$HallRoomNo."',".$StudentID.")";
			$query_bloodgroup = " insert into bloodgroup values (".$StudentID.",'".$BloodGroup."')";
			
			
			
			
			
			
			$stid_donor = oci_parse($conn, $query_donor);
			$stid_student = oci_parse($conn, $query_student);
			$stid_bloodgroup = oci_parse($conn, $query_bloodgroup);
			if ($query_run =  oci_execute($stid_bloodgroup) && $query_run =  oci_execute($stid_donor) && $query_run =  oci_execute($stid_student)    ) {
			      echo "Data Inserted";
		    }
		    else echo "Add of new donor failed";
			
 }

 
?>















<form class="form-horizontal" action="student.php" method="GET">
            <fieldset>
            
            <!-- Form Name -->  
            <legend align="center">Add Student</legend>
           
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for=StudentID">StudentID</label>  
              <div class="col-md-4">
              <input id="StudentID" name="StudentID" type="text" placeholder="StudentID"  value="<?php if (isset($_GET['StudentID'])) echo $StudentID;?>" class="form-control input-md" required>
                
              </div>
            </div>
             <div class="form-group">
            
               <label class="col-md-4 control-label" for="Firstname">First Name</label>  
              <div class="col-md-4">
              <input id="Firstname" name="Firstname" type="text" placeholder="Firstname"  value="<?php if (isset($_GET['Firstname'])) echo $Firstname ;?>" class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Lastname">Last Name</label>  
              <div class="col-md-4">
              <input id="Lastname" name="Lastname" type="text" placeholder="Lastname"  value="<?php if (isset($_GET['Lastname'])) echo $Lastname;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Batch">Batch</label>  
              <div class="col-md-4">
              <input id="Batch" name="Batch" type="text" placeholder="Batch"  value="<?php if (isset($_GET['Batch'])) echo $Batch;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-md-4 control-label" for="DEPT">DEPT</label>  
              <div class="col-md-4">
              <input id="DEPT" name="DEPT" type="text" placeholder="DEPT"  value="<?php if (isset($_GET['DEPT'])) echo $DEPT;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            
            <!-- Select Basic -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="zone">Zone</label>
              <div class="col-md-4">
                <select id="zone" name="zone"  class="form-control">
                  <option value="ZoneA"  >ZoneA</option>
                  <option value="ZoneB"  >ZoneB</option>
                  <option value="ZoneC"  >ZoneC</option>
                </select>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-md-4 control-label" for="unit">Unit</label>
              <div class="col-md-4">
                <select id="unit" name="unit" class="form-control">
                  <option value="UnitA" >UnitA</option>
                  <option value="UnitB"  >UnitB</option>
                  <option value="UnitC"  >UnitC</option>
                 
                </select>
              </div> 
            </div>
            
                        <div class="form-group">
              <label class="col-md-4 control-label" for="BloodGroup">Blood Group</label>
              <div class="col-md-4">
                <select id="BloodGroup" name="BloodGroup" class="form-control">
                  <option value="A+ve">A+ve</option>
                  <option value="AB+ve">AB+ve</option>
                  <option value="B+ve">B+ve</option>
                  <option value="O+ve">O+ve</option>
                  <option value="A-ve">A-ve</option>
                  <option value="AB-ve">AB-ve</option>
                  <option value="B-ve">B-ve</option>
                  <option value="O-ve">O-ve</option>
                </select>
              </div>
            </div>
                        
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="HallRoomNo">HallRoomNo</label>  
              <div class="col-md-4">
              <input id="HallRoomNo" name="HallRoomNo" type="text" placeholder="HallRoomNo"  value="<?php if (isset($_GET['HallRoomNo'])) echo $HallRoomNo;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-md-4 control-label" for="LDD">LastDonationDate</label>  
              <div class="col-md-4">
              <input id="LDD" name="LDD" type="date" placeholder="LDD"  value="<?php // if (isset($_GET['LDD'])) echo $LDD;?>" class="form-control input-md" >
                
              </div>
            </div>
            
             
           
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="ContactNo">Contact No</label>  
              <div class="col-md-4">
              <input id="ContactNo" name="ContactNo" type="text" placeholder="ContactNo"  value="<?php if (isset($_GET['ContactNo'])) echo $ContactNo;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Select Basic -->
           
            
           
            
            <!-- Button -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="singlebutton"></label>
              <div class="col-md-4">
                <button id="singlebutton" name="singlebutton" class="btn btn-primary">Sign In</button>
              </div>
            </div>
            
            
            
            </fieldset>
            </form>
