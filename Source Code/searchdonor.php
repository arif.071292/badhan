<title>Search</title>
<link rel="stylesheet" href="theme.css"/>

<link rel="stylesheet" href="bootstrap.css"/>
<?php 
	include('header.php');
	if(loggedin() == false)
	{
		header("Location: login.php");
	}
	include_once('Addheader.php');
	echo "Welcome ".$_SESSION['username']."<br>";
	echo "Unit: ".$_SESSION['Unit']."<br>";
	echo "Zone: ".$_SESSION['Zone'];
	
?>


<form class="form-inline" action="searchdonor.php" method="POST">
<fieldset>


<!-- Form Name -->
<legend>Search Available Donor. You can Search donor of your unit only</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="BloodGroup">Blood Group</label>
  <div class="col-md-4">
    <select id="BloodGroup" name="BloodGroup" class="form-control">
      <option value="A+ve">A+ve</option>
      <option value="AB+ve">AB+ve</option>
      <option value="B+ve">B+ve</option>
      <option value="O+ve">O+ve</option>
      <option value="A-ve">A-ve</option>
      <option value="AB-ve">AB-ve</option>
      <option value="B-ve">B-ve</option>
      <option value="O-ve">O-ve</option>
    </select>
  </div>
</div>

<input type="submit" class="btn btn-primary btn-default" value="Search" />

</fieldset>
</form>


<?php

if(isset($_POST['BloodGroup']) && !empty($_POST['BloodGroup']))
{
	$bloodgroup = $_POST['BloodGroup'];
	$query = " select firstname,lastname,contactno,dept,lastdonationdate,donationcount from donor natural join bloodgroup where bloodtype = '".$bloodgroup."' and ((trunc(sysdate) - lastdonationDate)>120 OR LASTDONATIONDATE IS NULL) and zone = '".strtoupper($_SESSION['Zone'])."' and unit = '".strtoupper($_SESSION['Unit'])."' and status = 'Y'";
	
	
	$stid = oci_parse($conn,$query);
	if($query_run = oci_execute($stid)) {
		$ncols = oci_num_fields($stid);
		
		echo "<h2 align=\"center\">Search Result For ".$bloodgroup." in ".$_SESSION['Unit']."</h2>";
		
		
		echo "<div class=\"container\">
			 	<div class=\"row clearfix\">
					<div class=\"col-md-12 column\">
						<table class=\"table\"> <tr> \n";
		for ($i = 1; $i <= $ncols; ++$i) {
			 
			 $colname = oci_field_name($stid, $i);
			 echo "  <th><b>".htmlentities($colname, ENT_QUOTES)."</b></th>\n";
			 
			}
			echo "</tr>\n";
		//echo "</table>\n";
		
		//echo "<table class=\"table\"> <tr> \n";
		while ($row = oci_fetch_array($stid, OCI_ASSOC+OCI_RETURN_NULLS)) {
				echo "<tr>\n";
				
				foreach ($row as $item) {
					echo "<td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
					
				}
				echo "</tr>\n";
		}
		echo "</table>\n";
			
	}
}


?>