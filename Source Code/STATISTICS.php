<title> Donation Statistics </title>



<link rel="stylesheet" href="bootstrap.css"/>
<?php include('header.php');?>	
<?php include("databaseconnection.php"); ?>

<head>
<div class="stat_donor">

<form class="form-horizontal" action="statistics.php" method="get">
<fieldset>

<!-- Form Name -->
<legend>Statistics</legend>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="stat"></label>
  <div class="col-md-4">
    <select id="stat" name="stat" class="form-control">
      <option value="Loc">Location</option>
      <option value="Don">Donation</option>
    </select>
  </div>
</div>
<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submit" class="btn btn-primary">Submit</button>
  </div>
</div>

</fieldset>
</form>


<?php
if (isset($_GET['stat']))
{
	$stat = $_GET['stat'];
	if($stat =='Loc') 
	{
		$query_loc = "select HOSPITALNAME, count(*) as TOTAL from Hospital NATURAL JOIN PATIENT group by HOSPITALNAME ORDER BY HOSPITALNAME";
		$stid_loc = oci_parse($conn,$query_loc);
		if($query_run = oci_execute($stid_loc) )
		{
			$ncols = oci_num_fields($stid_loc);
		
		echo "<h2 align=\"center\">Location Statistics</h2>";
		echo "<table class=\"table\" align=\"center\"> <tr> \n";
		for ($i = 1; $i <= $ncols; ++$i) {
			 
			 $colname = oci_field_name($stid_loc, $i);
			 echo "  <th><b>".htmlentities($colname, ENT_QUOTES)."</b></th>\n";
			 
			}
			echo "</tr>\n";
		//echo "</table>\n";
		
		//echo "<table class=\"table\"> <tr> \n";
		while ($row = oci_fetch_array($stid_loc, OCI_ASSOC+OCI_RETURN_NULLS)) {
				echo "<tr>\n";
				
				foreach ($row as $item) {
					echo "<td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
					
				}
				echo "</tr>\n";
		}
		echo "</table>\n";
		}
	}
	else if($stat =='Don') 
	{
		$query_don = "SELECT ZONE,SUM(DONATIONCOUNT) AS TOTALDONATION FROM DONOR GROUP BY ZONE ORDER BY ZONE";
		$stid_don = oci_parse($conn,$query_don);
		if($query_run = oci_execute($stid_don) )
		{
			$ncols = oci_num_fields($stid_don);
		
		echo "<h2 align=\"center\">Donation Statistics</h2>";
		echo "<table class=\"table\" align=\"center\"> <tr> \n";
		for ($i = 1; $i <= $ncols; ++$i) {
			 
			 $colname = oci_field_name($stid_don, $i);
			 echo "  <th><b>".htmlentities($colname, ENT_QUOTES)."</b></th>\n";
			 
			}
			echo "</tr>\n";
		//echo "</table>\n";
		
		//echo "<table class=\"table\"> <tr> \n";
		while ($row = oci_fetch_array($stid_don, OCI_ASSOC+OCI_RETURN_NULLS)) {
				echo "<tr>\n";
				
				foreach ($row as $item) {
					echo "<td>" . ($item !== null ? htmlentities($item, ENT_QUOTES) : "&nbsp;") . "</td>\n";
					
				}
				echo "</tr>\n";
		}
		echo "</table>\n";
		}
	}
	
}


?>




 
</head>

<body>
</body>
</html>
