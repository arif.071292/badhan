<link rel="stylesheet" href="theme.css"/>

<link rel="stylesheet" href="bootstrap.css"/>
<?php include('header.php')?>	
<?php include("databaseconnection.php"); ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Registration</title>
</head>


<?php
if(isset($_POST["UserName"]) && isset($_POST["passwordinput"]) && isset($_POST["passwordinputagain"]))
{
	$username = strtolower($_POST["UserName"])  ;
	$firstname = strtolower( $_POST["FirstName"])  ;
	$lastname = strtolower( $_POST["LastName"])  ;
	$unit = strtoupper($_POST["unit"])  ;
	$zone = strtoupper($_POST["zone"])  ;
	$ContactNo = $_POST["contactno"]  ;
	$DesignationID = $_POST["DesignationID"]  ;
	$Batch = $_POST["Batch"]  ;
	$StudentNo =$_POST["StudentNo"]  ;
	$pass = md5($_POST["passwordinput"])  ;
	$pass_again = md5($_POST["passwordinputagain"]);
	
	if($pass != $pass_again) 
	{
		echo "passwords dont match";
		die();
	}
	else
	{
		$query = "select username from login_data where username = '".$username."'";
		$stid = oci_parse($conn, $query);
		if ($query_run =  oci_execute($stid)) {
			//echo "successs";
			$results=array();
			$numrows = oci_fetch_all($stid, $results, null, null, OCI_FETCHSTATEMENT_BY_ROW);
			
			oci_free_statement($stid);
			if($numrows>0)
			{
				echo "This user name already exists";
			}
			else
			{
				$query_insert = "insert into login_data(username,pass_w) values('".$username."','".$pass."')";
				$query_pending_data= "insert into pending_account values ('".$firstname."','".$lastname."','".$zone."','".$unit."','".$Batch."','".$ContactNo."',$DesignationID,'".$username."')";
				$query_member_insert = "insert into member(memberid,firstname,lastname,unit,zone,contactno,designationid3,username) values ('".$StudentNo."','".$firstname."','".$lastname."','".$unit."','".$zone."','".$ContactNo."',$DesignationID,'".$username."')";
				//echo $query_member_insert;
				//die();
				
				
				
				
				
				
				
				//echo $query_member_insert."\n";
				
				
				$stid = oci_parse($conn, $query_insert);
				$stid_pending = oci_parse($conn, $query_pending_data);
				$stid_member_insert = oci_parse($conn, $query_member_insert);
				if ( ($query_run_insert = oci_execute($stid_member_insert)) && ($query_run =  oci_execute($stid)) && ($query_run_pending = oci_execute($stid_pending))    ) {
					//echo "signup complete";
					//require_once( 'core.inc.php');  ($query_run =  oci_execute($stid))
					
					//header( "Location: login.php");
					header( "Location: registrationcomplete.php");
				}
				else
				{
					if(!$query_member_insert) echo "There may be a problem in memberID";
					else if (!$query_run) echo "username may be existing.choose another username";
					
					// here goes the code if any of the above code doesnot run
				}
				
			}
		}
	}
	
	
	
	
}



?>

<body>
            <form class="form-horizontal" action="register.php" method="post">
            <fieldset>
            
            <!-- Form Name -->
            <legend align="center">Register</legend>
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="UserName">User Name</label>  
              <div class="col-md-4">
              <input id="UserName" name="UserName" type="text" placeholder="UserName"  value="<?php if (isset($_POST['UserName'])) echo $username;?>" class="form-control input-md" required>
                
              </div>
            </div>
             <div class="form-group">
            
               <label class="col-md-4 control-label" for="FirstName">First Name</label>  
              <div class="col-md-4">
              <input id="FirstName" name="FirstName" type="text" placeholder="First Name"  value="<?php if (isset($_POST['FirstName'])) echo $firstname ;?>" class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="LastName">Last Name</label>  
              <div class="col-md-4">
              <input id="LastName" name="LastName" type="text" placeholder="Last Name"  value="<?php if (isset($_POST['LastName'])) echo $lastname;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Select Basic -->
            
            
            <!-- Select Basic -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="zone">Zone</label>
              <div class="col-md-4">
                <select id="zone" name="zone"  class="form-control">
                  <option value="ZoneA" <?php if (isset($_POST["zone"]) && $_POST["zone"] == "ZoneA") echo "selected='selected'";?> >ZoneA</option>
                  <option value="ZoneB" <?php if (isset($_POST["zone"]) && $_POST["zone"] == "ZoneB") echo "selected='selected'";?> >ZoneB</option>
                  <option value="ZoneC" <?php if (isset($_POST["zone"]) && $_POST["zone"] == "ZoneC") echo "selected='selected'";?> >ZoneC</option>
                </select>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-md-4 control-label" for="unit">Unit</label>
              <div class="col-md-4">
                <select id="unit" name="unit" class="form-control">
                  <option value="UnitA" <?php if (isset($_POST["unit"]) && $_POST["unit"] == "UnitA") echo "selected='selected'";?>>UnitA</option>
                  <option value="UnitB" <?php if (isset($_POST["unit"]) && $_POST["unit"] == "UnitB") echo "selected='selected'";?> >UnitB</option>
                  <option value="UnitC" <?php if (isset($_POST["unit"]) && $_POST["unit"] == "UnitC") echo "selected='selected'";?> >Unitc</option>
                 
                </select>
              </div>
            </div>
            
             <div class="form-group">
              <label class="col-md-4 control-label" for="DesignationID">DesignationID</label>
              <div class="col-md-4">
              			<input id="DesignationID" name="DesignationID" type="text" placeholder="DesignationID"  value="<?php if (isset($_POST['DesignationID'])) echo $DesignationID;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <div align="center"> <a href="designationID.php" target="new">Click here</a> to see designation id</div>
            <div class="form-group">
                  <label class="col-md-4 control-label" for="StudentNo">StudentNo</label>  
                  <div class="col-md-4">
                  <input id="StudentNo" name="StudentNo" type="text" placeholder="StudentNo"  value="<?php if (isset($_POST['StudentNo'])) echo $StudentNo;?>"  class="form-control input-md" required>
                    
				  </div>
			</div>
            
            <div class="form-group">
              <label class="col-md-4 control-label" for="Batch">Batch</label>  
              <div class="col-md-4">
              <input id="Batch" name="Batch" type="text" placeholder="Batch"  value="<?php if (isset($_POST['Batch'])) echo $Batch;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="contactno">Contact No</label>  
              <div class="col-md-4">
              <input id="contactno" name="contactno" type="text" placeholder="Contact No"  value="<?php if (isset($_POST['contactno'])) echo $ContactNo;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Select Basic -->
           
            
            <!-- Password input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="passwordinput">Password</label>
              <div class="col-md-4">
                <input id="passwordinput" name="passwordinput" type="password" placeholder="" class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Password input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="passwordinputagain">Retype Password</label>
              <div class="col-md-4">
                <input id="passwordinputagain" name="passwordinputagain" type="password" placeholder="" class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Button -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="singlebutton"></label>
              <div class="col-md-4">
                <button id="singlebutton" name="singlebutton" class="btn btn-primary">Sign In</button>
              </div>
            </div>
            
            
            
            </fieldset>
            </form>




</body>
</html>

