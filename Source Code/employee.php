<link rel="stylesheet" href="theme.css"/>

<link rel="stylesheet" href="bootstrap.css"/>
<?php include('header.php')?>	
<?php include("databaseconnection.php"); ?>
<?php include_once("addheader.php"); ?>

<?php
if(!isset($_SESSION['id'])) { header("Location: index.php");}
?>


<?php
	 if ( isset($_GET["EmployeeID"]) && isset($_GET["Firstname"]) && isset($_GET["Lastname"])  && isset($_GET["JobTitle"])  && isset($_GET["DEPT"]) && isset($_GET["zone"]) && isset($_GET["unit"]) && isset($_GET["BloodGroup"]) && isset($_GET["ContactNo"])  && isset($_GET["LDD"])  )
 {
	 $EmployeeID =   $_GET["EmployeeID"];
	 $Firstname = strtolower($_GET["Firstname"]);
	 $Lastname = strtolower($_GET["Lastname"]);
	 $DEPT = $_GET["DEPT"];
	 $zone = strtoupper($_GET["zone"]);
	 $JobTitle =  $_GET["JobTitle"];
			
	 $unit = strtoupper($_GET["unit"]);
	 $BloodGroup = $_GET["BloodGroup"];
	 $LastDonationDate = $_GET["LDD"];
	 $date = new DateTime($LastDonationDate);
	 $ldd =  $date->format('d-M-Y');
	 $ContactNo = $_GET["ContactNo"];
	 
	 
	 
	 
			
			
			$query = "select donorid from donor where donorid = $EmployeeID";
			$stid = oci_parse($conn,$query);
			if($query_run = oci_execute($stid))
			{
				$results=array();
				$numrows = oci_fetch_all($stid, $results, null, null, OCI_FETCHSTATEMENT_BY_ROW);
				if($numrows > 0)
				{
					echo "donor of this EmployeeID is already inserted";
					die();
				}
				oci_free_statement($stid);
			}
			
			
					
		     $query_donor = "insert into donor(DonorID,Firstname,Lastname,DEPT,Zone,Unit,lastdonationdate,ContactNo,B_ID) values ( ".$EmployeeID.",' ".             $Firstname." ',' ".$Lastname."' , ' ".$DEPT." ',' ".$zone." ',' ".$unit." ',' ".$ldd." ',' "
			.$ContactNo."',".$EmployeeID.")";
			
			$query_employee = " insert into employee values (".$EmployeeID.",'".$JobTitle."',".$EmployeeID.")";
			
			$query_bloodgroup = " insert into bloodgroup values (".$EmployeeID.",'".$BloodGroup."')";
			
			/*echo $query_bloodgroup;
			echo $query_donor;
			echo $query_employee;
			die();
			*/
			$stid_donor = oci_parse($conn, $query_donor);
			$stid_employee = oci_parse($conn, $query_employee);
			$stid_bloodgroup = oci_parse($conn, $query_bloodgroup);
			
			if ($query_run =  oci_execute($stid_bloodgroup) && $query_run =  oci_execute($stid_donor) && $query_run =  oci_execute($stid_employee)    ) {
			      echo "Data Inserted";
		    }
		    else echo "Add of new donor failed";
 }
 
?>















<form class="form-horizontal" action="employee.php" method="GET">
            <fieldset>
            
            <!-- Form Name -->  
            <legend align="center">Add Employee</legend>
           
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for=EmployeeID">EmployeeID</label>  
              <div class="col-md-4">
              <input id="EmployeeID" name="EmployeeID" type="text" placeholder="EmployeeID"  value="<?php if (isset($_GET['Employee'])) echo $Employee;?>" class="form-control input-md" required>
                
              </div>
            </div>
             <div class="form-group">
            
               <label class="col-md-4 control-label" for="Firstname">First Name</label>  
              <div class="col-md-4">
              <input id="Firstname" name="Firstname" type="text" placeholder="Firstname"  value="<?php if (isset($_GET['Firstname'])) echo $Firstname ;?>" class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="Lastname">Last Name</label>  
              <div class="col-md-4">
              <input id="Lastname" name="Lastname" type="text" placeholder="Lastname"  value="<?php if (isset($_GET['Lastname'])) echo $Lastname;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="JobTitle">JobTitle</label>  
              <div class="col-md-4">
              <input id="JobTitle" name="JobTitle" type="text" placeholder="JobTitle"  value="<?php if (isset($_GET['JobTitle'])) echo $JobTitle;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-md-4 control-label" for="DEPT">DEPT</label>  
              <div class="col-md-4">
              <input id="DEPT" name="DEPT" type="text" placeholder="DEPT"  value="<?php if (isset($_GET['DEPT'])) echo $DEPT;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            
            <!-- Select Basic -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="zone">Zone</label>
              <div class="col-md-4">
                <select id="zone" name="zone"  class="form-control">
                  <option value="ZoneA"  >ZoneA</option>
                  <option value="ZoneB"  >ZoneB</option>
                  <option value="ZoneC"  >ZoneC</option>
                </select>
              </div>
            </div>
            
            <div class="form-group">
              <label class="col-md-4 control-label" for="unit">Unit</label>
              <div class="col-md-4">
                <select id="unit" name="unit" class="form-control">
                  <option value="UnitA" >UnitA</option>
                  <option value="UnitB"  >UnitB</option>
                  <option value="UnitC"  >UnitC</option>
                 
                </select>
              </div> 
            </div>
            
                        <div class="form-group">
              <label class="col-md-4 control-label" for="BloodGroup">Blood Group</label>
              <div class="col-md-4">
                <select id="BloodGroup" name="BloodGroup" class="form-control">
                  <option value="A+ve">A+ve</option>
                  <option value="AB+ve">AB+ve</option>
                  <option value="B+ve">B+ve</option>
                  <option value="O+ve">O+ve</option>
                  <option value="A-ve">A-ve</option>
                  <option value="AB-ve">AB-ve</option>
                  <option value="B-ve">B-ve</option>
                  <option value="O-ve">O-ve</option>
                </select>
              </div>
            </div>
                        
            <!-- Text input-->
            
            
            <div class="form-group">
              <label class="col-md-4 control-label" for="LDD">LastDonationDate</label>  
              <div class="col-md-4">
              <input id="LDD" name="LDD" type="date" placeholder="LDD"  value="<?php if (isset($_GET['LDD'])) echo $LDD;?>" class="form-control input-md" required>
                
              </div>
            </div>
            
             
           
            
            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="ContactNo">Contact No</label>  
              <div class="col-md-4">
              <input id="ContactNo" name="ContactNo" type="text" placeholder="ContactNo"  value="<?php if (isset($_GET['ContactNo'])) echo $ContactNo;?>"  class="form-control input-md" required>
                
              </div>
            </div>
            
            <!-- Select Basic -->
           
            
           
            
            <!-- Button -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="singlebutton"></label>
              <div class="col-md-4">
                <button id="singlebutton" name="singlebutton" class="btn btn-primary">Sign In</button>
              </div>
            </div>
            
            
            
            </fieldset>
            </form>
