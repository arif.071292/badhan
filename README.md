BADHAN DATABASE

Badhan is an educational institute based voluntary blood donor organization. Each university is considered as a zone and each hall of a university is considered as a unit. Educational institutes which don’t have residential hall are considered as a family. Badhan BUET Zone has 7 Unit. Our project goal is to create a central Badhan database so that whole Badhan management system can be automated.

People who are in needs of  blood come to Badhan office. At first Badhan member verifies the them and check their blood requisition form. After verification Badhan member  search the database for available donor. When a donor found, he/she is requested to donate blood to the respective hospital or Blood Bank. After donating blood, Badhan member update his/her donation information.
